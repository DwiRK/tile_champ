﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.LuisPedroFonseca.ProCamera2D
{
	public class CameraController : MonoBehaviour {
		private ProCamera2DTransitionsFX transitionsFX;
		private ProCamera2DContentFitter contentFitter;
		private GameObject camera;
		private GameObject player;

		// Use this for initialization
		void Start () {
			transitionsFX = GameObject.FindObjectOfType<ProCamera2DTransitionsFX>();
			contentFitter = GameObject.FindObjectOfType<ProCamera2DContentFitter>();
			player = GameObject.FindGameObjectWithTag("Player");
		}
		
		// Update is called once per frame
		void Update () {
			
		}

		public void startTransition()
		{
			transitionsFX.TransitionEnter();
		}

		public void changeSize()
		{
			contentFitter.enabled = true;
		}

		public void addCameraTarget()
		{
			ProCamera2D.Instance.AddCameraTarget(player.transform);
		}
	}
}
