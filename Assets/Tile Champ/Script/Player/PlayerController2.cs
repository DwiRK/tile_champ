﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController2 : MonoBehaviour {

    private float xInput, yInput;
    public int speed = 1;
    private Rigidbody2D body;
    public Animator animator;
    private bool isMoving;


	// Use this for initialization
	void Start () {
        body = GetComponent<Rigidbody2D>();
        // animator = GetComponent<Animator>();
        isMoving = false;
        // isAttacking = false;
    }
	
	// Update is called once per frame
	void Update () {

        // Attack Test
        // if(Input.GetButtonDown("Fire1"))
        // {
        //     animator.SetTrigger("Attack");
        // }

        // Get input
        xInput = Input.GetAxisRaw("Horizontal");
        yInput = Input.GetAxisRaw("Vertical");

        // If IsMoving, Move with 3d Vector
        isMoving = (xInput != 0 || yInput != 0);

        if (isMoving)
        {
            var moveVector = new Vector3(xInput , yInput, 0);   

            // BAD MOVEMENT - Circumvents RigidBody2D Physics
            // transform.position += moveVector * speed * Time.deltaTime;
            //

            // CORRECT MOVEMENT
            body.MovePosition(new Vector2((transform.position.x + moveVector.x * speed * Time.deltaTime),
                   transform.position.y + moveVector.y * speed * Time.deltaTime));


            // animator.SetFloat("xInput", xInput);
            // animator.SetFloat("yInput", yInput);
        }

        // animator.SetBool("isMoving", isMoving);
    }
}
