﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	private PlayerMovement playerMovement;
	private Animator animator;

	// Use this for initialization
	void Start () {
		// if(playerMovement == null)
		// {
		// 	return;
		// }

		playerMovement = gameObject.GetComponent<PlayerMovement>();
		animator = gameObject.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		animator.SetFloat("Speed", Mathf.Abs(playerMovement.joystick.Horizontal));
		animator.SetFloat("Speed", Mathf.Abs(playerMovement.joystick.Vertical));

		if(playerMovement.joystick.Horizontal < -0.1f)
		{
			gameObject.GetComponent<SpriteRenderer>().flipX =true;
		}

		if(playerMovement.joystick.Horizontal > 0.1f)
		{
			gameObject.GetComponent<SpriteRenderer>().flipX = false;
		}
	}

	private void FixedUpdate() {
		if(Input.GetKeyDown(KeyCode.E))
		{
			gameObject.GetComponent<Rigidbody2D>().AddForce(transform.forward * 50f);
		}
	}
}
