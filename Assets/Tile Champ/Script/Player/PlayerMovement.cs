﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;

public class PlayerMovement : MonoBehaviour {
	private PlayerController playerController;
	private PauseLayout pauseGame;

	public Joystick joystick;
	public float moveSpeed;
	private float theSpeed;

	// Reference Component
	private Rigidbody2D rigidbody2D;
	public Vector2 moveVelocity;
	public Vector2 moveInput;

	// Use this for initialization
	void Start () {
		rigidbody2D = gameObject.GetComponent<Rigidbody2D>();

		playerController = GetComponent<PlayerController>();
		pauseGame = GameObject.FindObjectOfType<PauseLayout>();

		theSpeed = moveSpeed;
	}
	
	// Update is called once per frame
	void Update () {
		if(joystick == null)
		{
			return;
		}
		// Vector2 moveInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
		moveInput = new Vector2(joystick.Horizontal, joystick.Vertical);
		moveVelocity = moveInput.normalized * moveSpeed;
	}

	private void FixedUpdate() {
		rigidbody2D.MovePosition(rigidbody2D.position + moveVelocity * Time.fixedDeltaTime);
	}

	public float getTheSpeed()
	{
		return theSpeed;
	}
}
