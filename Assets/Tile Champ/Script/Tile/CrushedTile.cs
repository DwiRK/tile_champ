﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrushedTile : MonoBehaviour {
	public int playerHit = 0;
	public GameObject dieArea;
	private int hit;
	private bool startFade = false;
	public float countdown = 3;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(startFade)
		{
			countdown -= Time.deltaTime;

			if(countdown <= 0)
			{
				gameObject.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 255f, 0);

				dieArea.SetActive(true);

				startFade = false;
			}
		}
	}

	private void OnTriggerEnter2D(Collider2D obj) {
		if(hit == playerHit+1)
		{
			startFade = true;
		}
		else
		{
			hit++;
		}
	}
}
