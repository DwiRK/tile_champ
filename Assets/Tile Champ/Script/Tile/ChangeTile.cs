﻿	 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;

public class ChangeTile : MonoBehaviour {

	/*Note : 
		Tile Type : 
		TYPE 1 = normal Tile
		TYPE 2 = normal Tile, Chrused Tile
		TYPE 3 = normal Tile, Chrused Tile, Tower Tile, Stone Tile
	 */

	// Tile Component
	public enum Tile {TYPE1, TYPE2, TYPE3}
	public Tile tile = Tile.TYPE1;
	public SpriteRenderer[] tileSprite;
	private TileController tileController;
	
	// Tile Identity
	private int tileNumber = 0;
	private bool towerRight;
	public bool opacity = false;		// define that the tile opacity is on or off
	private bool tilesOff = false;		// check the opacity
	private bool canCount = true;		// define that the script count the active ti;e
	private bool canChangeTile = false; // define thet the tile can be change

	// Use this for initialization
	void Start () {
		tileController = GameObject.FindObjectOfType<TileController>();

		if(tileSprite == null)
		{
			return;
		}

		gameObject.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 255f, 0.5f);
	}
	
	// Update is called once per frame
	void Update () {
		if(tileController.getCountTile() != tileController.tileAmount)
		{
			canChangeTile = true;
		}
		else if(tileController.getCountTile() == tileController.tileAmount && !opacity)
		{
			canChangeTile = false;
		}

		if(tilesOff && tileController.getCountTile() != 0)
		{
			tileController.setCountTile(-1);

			tilesOff = false;

			canCount = true;
		}
	}

	private void OnMouseDown() 
	{
		if(tileController.getCanChange())
		{
			if(canChangeTile)
			{
				if(tile == Tile.TYPE1)
				{
					tileType1();
				} 
				else if(tile == Tile.TYPE2)
				{
					tileType2();
				}
				else if(tile == Tile.TYPE3)
				{
					tileType3();
				}

				if(canCount)
				{
					tileController.setCountTile(1);
					canCount = false;
				}
			}
		}
	}

	private void tileType1()
	{
		if(!opacity)
		{
			gameObject.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 255f, 1f);
			opacity = true;
		}
		else
		{
			gameObject.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 255f, 0.5f);
			opacity = false;
			tilesOff = true;
		}
	}

	private void tileType2()
	{
		if(!opacity)
		{
			gameObject.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 255f, 1f);
			opacity = true;
		}
		else
		{
			if(tileNumber < tileSprite.Length)
			{
				if((tileNumber+1) == tileSprite.Length)
				{
					gameObject.GetComponent<SpriteRenderer>().sprite = tileSprite[0].sprite;
					gameObject.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 255f, 0.5f);
					
					tileNumber = 0;
					opacity = false;
					tilesOff = true;
				}
				else
				{
					gameObject.GetComponent<SpriteRenderer>().sprite = tileSprite[tileNumber+1].sprite;
					
					tileNumber++;
				}
			}
		}
	}

	private void tileType3()
	{
		if(!opacity)
		{
			gameObject.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 255f, 1f);
			opacity = true;
		}
		else
		{
			if(tileNumber < tileSprite.Length)
			{
				if((tileNumber+1) == tileSprite.Length)
				{
					gameObject.GetComponent<SpriteRenderer>().sprite = tileSprite[0].sprite;
					gameObject.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 255f, 0.5f);

					transform.GetChild(0).gameObject.SetActive(false);
					
					tileNumber = 0;
					opacity = false;
					tilesOff = true;
				}
				else
				{
					if((tileNumber+1) == 1)
					{
						gameObject.GetComponent<SpriteRenderer>().sprite = tileSprite[tileNumber+1].sprite;
						transform.GetChild(0).gameObject.SetActive(true);
						
						tileNumber++;
					}
					else if(towerRight) 
					{
						transform.GetChild(1).gameObject.SetActive(false);
						transform.GetChild(2).gameObject.SetActive(true);

						gameObject.GetComponent<SpriteRenderer>().sprite = tileSprite[2].sprite;

						towerRight = false;

						tileNumber++;
					}
					else if(tileNumber+1 == 2)
					{
						transform.GetChild(0).gameObject.SetActive(false);
						transform.GetChild(1).gameObject.SetActive(true);

						gameObject.GetComponent<SpriteRenderer>().sprite = tileSprite[2].sprite;

						towerRight = true;
						tileNumber = 1;
					}
					else
					{
						transform.GetChild(2).gameObject.SetActive(false);

						gameObject.GetComponent<SpriteRenderer>().sprite = tileSprite[3].sprite;

						tileNumber++;
					}
				}
			}
		}
	}
}
