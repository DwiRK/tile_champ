﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.LuisPedroFonseca.ProCamera2D
{
	public class TileController : MonoBehaviour {
		// script reference
		private CameraController cameraController;

		public GameObject player;
		public GameObject playerJoystick;
		public GameObject playBtn;
		public GameObject tileUI;
		private GameObject[] tile;
		public SpriteRenderer[] tileSprite;

		public int tileAmount = 0;
		private int countTile = 0;
		private bool canChange = true;
		private bool isPlaying;

		// Use this for initialization
		void Start () {
			cameraController = GameObject.FindObjectOfType<CameraController>();
		}
		
		// Update is called once per frame
		void Update () {
			
		}

		public void activateTile()
		{
			tileCollider();
			updateTile();
			

			cameraController.addCameraTarget();
			cameraController.startTransition();
			cameraController.changeSize();

			canChange = false;
			isPlaying = true;
			
			playerJoystick.SetActive(true);
			playBtn.SetActive(false);
			tileUI.SetActive(false);
		}

		public void tileCollider()
		{
			tile = GameObject.FindGameObjectsWithTag("Platforms");

			foreach (var checkTile in tile)
			{
				if(checkTile.GetComponent<ChangeTile>().opacity)
				{
					checkTile.GetComponent<BoxCollider2D>().isTrigger = true;
					
					Debug.Log("Tile Colider");
				}

				if(!checkTile.GetComponent<ChangeTile>().opacity)
				{
					checkTile.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 255f, 0);
				}
			}
		}

		public void updateTile()
		{
			tile = GameObject.FindGameObjectsWithTag("Platforms");

			foreach (var checkTile in tile)
			{	
				// check if stone tile active then enable scipt
				if(checkTile.GetComponent<SpriteRenderer>().sprite == tileSprite[1].sprite)
				{
					checkTile.GetComponent<StoneTile>().enabled = true;

					Debug.Log("StoneTile");
				}

				// check if cloud tile active then enable scipt
				if(checkTile.GetComponent<SpriteRenderer>().sprite == tileSprite[2].sprite)
				{
					checkTile.GetComponent<CloudyTile>().enabled = true;

					Debug.Log("CloudTile");
				}

				// check if tower tile active then enable scipt
				if(checkTile.GetComponent<SpriteRenderer>().sprite == tileSprite[3].sprite)
				{
					checkTile.GetComponent<BoxCollider2D>().isTrigger = false;

					Debug.Log("TowerTile");
				}

				// check if chrused tile active then enable scipt
				if(checkTile.GetComponent<SpriteRenderer>().sprite == tileSprite[4].sprite)
				{
					checkTile.GetComponent<CrushedTile>().enabled = true;
					Debug.Log("Chrused Tile");
				}
			}
		}

		public void setCountTile(int countTile)
		{
			this.countTile += countTile;
		}

		public int getCountTile()
		{
			return countTile;
		}

		public void setCanChange(bool canChange)
		{
			this.canChange = canChange;
		}

		public bool getCanChange()
		{
			return canChange;
		}

		public bool getIsPlaying()
		{
			return isPlaying;
		}
	}
}
