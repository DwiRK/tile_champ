﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MudTile : MonoBehaviour {

	private GameObject player;

	public float addFor;
	private bool spliperry;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
		if(spliperry)
		{
			player.GetComponent<Rigidbody2D>().AddForce(transform.forward * addFor);

			Debug.Log("Slipperry");
		}
		else 
		{
			return;
		}
	}

	private void FixedUpdate() {
		
	}

	private void OnTriggerEnter2D(Collider2D obj) {
		if(obj.gameObject.tag == "Player")
		{
			spliperry = true;
		}
	}
}
