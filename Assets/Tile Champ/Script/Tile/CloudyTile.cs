﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudyTile : MonoBehaviour {
	public GameObject dieArea;
	public float disappear;
	private float disCountdown;
	private bool startCount = true;

	// Use this for initialization
	void Start () {
		gameObject.GetComponent<BoxCollider2D>().isTrigger = true;

		disCountdown = disappear;
	}
	
	// Update is called once per frame
	void Update () {
		if(startCount)
		{
			disCountdown -= Time.deltaTime;
		}

		if(disCountdown <= 0)
		{
			gameObject.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 255f, 0);
			dieArea.SetActive(true);

			if(disCountdown <= (disappear*-1))
			{
				startCount = false;

				gameObject.GetComponent<SpriteRenderer>().color = new Color(255f, 255f, 255f, 1f);
				dieArea.SetActive(false);

				disCountdown = disappear;

				startCount = true;
			}
		}
	}
}
