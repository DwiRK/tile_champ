﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class RestartGame : MonoBehaviour {

	public void restartGame() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		
		// Time.timeScale = 1f;
	}
}
