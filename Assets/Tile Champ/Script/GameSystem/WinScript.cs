﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinScript : MonoBehaviour {
	public GameObject winLayout;
	public GameObject joystick;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter2D(Collider2D player) {
		if(player.gameObject.tag == "Player")
		{
			winLayout.SetActive(true);
			joystick.SetActive(false);
			Time.timeScale = 0;
		}
	}
}
