﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour {

	public GameObject pauseLayout;
	public GameObject optionBtn;
	public GameObject joystick;

	public void pauseGame()
	{
		pauseLayout.SetActive(true);

		Time.timeScale = 0;
	}

	public void resumeGame()
	{
		Time.timeScale = 1f;
	}
}
