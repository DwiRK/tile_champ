﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.LuisPedroFonseca.ProCamera2D
{
	public class PauseLayout : MonoBehaviour {
		private TileController tileController;
		private GameObject player;
		private GameObject[] tile;

		public GameObject pauseLayout;
		public GameObject pauseBtn;
		public GameObject joystick;
		public GameObject playBtn;

		public bool isPause;

		private void Start() {
			tileController = GameObject.FindObjectOfType<TileController>();
			player = GameObject.Find("Player");
		}

		public void pauseGame()
		{
			pauseLayout.SetActive(true);
			pauseBtn.SetActive(false);

			if(tileController.getIsPlaying())
			{
				joystick.SetActive(false);

				player.GetComponent<PlayerMovement>().moveSpeed = 0;
			}
			else
			{
				playBtn.SetActive(false);
				tileController.setCanChange(false);
			}

			player.GetComponent<Animator>().enabled = false;

			isPause = true;
		}

		public void resumeGame()
		{
			if(tileController.getIsPlaying())
			{
				joystick.SetActive(true);

				joystick.GetComponent<FixedJoystick>().handle.anchoredPosition = Vector2.zero;
				joystick.GetComponent<FixedJoystick>().inputVector = Vector2.zero;
				
				player.GetComponent<PlayerMovement>().moveSpeed = player.GetComponent<PlayerMovement>().getTheSpeed();
			}
			else 
			{
				playBtn.SetActive(true);
				tileController.setCanChange(true);
			}
			
			pauseBtn.SetActive(true);
			pauseLayout.SetActive(false);

			player.GetComponent<Animator>().enabled = true;
		}
	}
}
