﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;

public class GameOverM : MonoBehaviour {
	private GameObject player;
	public GameObject gameoverMenu;
	public GameObject joystick;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		player = GameObject.Find("Player");

		if(player == null)
		{
			gameoverMenu.SetActive(true);
			joystick.SetActive(false);
			// Time.timeScale = 0;
		}

		
	}
}
