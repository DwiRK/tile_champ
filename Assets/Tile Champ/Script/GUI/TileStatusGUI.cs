﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Com.LuisPedroFonseca.ProCamera2D;

namespace TMPro 
{
public class TileStatusGUI : MonoBehaviour {
	private TileController tileController;
	public TextMeshProUGUI tileText;

	// Use this for initialization
	void Start () {
		tileController = GameObject.FindObjectOfType<TileController>();
	}
	
	// Update is called once per frame
	void Update () {
		tileGUI();
	}

	private void tileGUI()
	{
		tileText.text = tileController.getCountTile() + " / " + tileController.tileAmount;
	}
}
}
