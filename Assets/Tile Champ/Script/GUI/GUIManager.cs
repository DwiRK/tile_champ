﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIManager : MonoBehaviour {

	public GameObject winLayout;
	public GameObject dieLayout;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void winScene()
	{
		winLayout.SetActive(true);
		Time.timeScale = 1;
	}

	public void dieScene()
	{
		dieLayout.SetActive(true);
		Time.timeScale = 1;
	}
}
